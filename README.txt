// $Id$

mgEveAuth - Eve online authentication layer for Drupal
----------------------------------------------------


Known incompatibilities
-----------------------

No incompatibilities with other modules are known to this date.


Maintainers
-----------
mgEveAuth is written by Michael Lehman - Drakos Wraith in game.

mgEveAuth was inspired by the 6.x EveAuth module developed by:
Helge Nordg�rd / aka: 'Helgur' in game - http://thestate.helges.net


